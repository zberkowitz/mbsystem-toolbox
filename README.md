# MB-System Fedora Toolbox Image

[Fedora Toolbox](https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/) uses
containers to provide an environment where development tools and libraries can
be installed and used.

This repository provides scripts to build OCI images based on fedora-toolbox
using [buildah](https://buildah.io).

# Requirements

- buildah
- OTPSnc and tpxo9 model

# Creating the Image

Download OTPSnc and the tpxo9 model.  Move the OTPSnc.tar.Z and
tpxo9_netcdf.tar.gz archives into this directory, they will be unpacked into
the image in the proper location.

Run the build script:

```
sh build-toolbox-image 
```

to create a new image `mbsystem-toolbox` tagged with the MB-System release.

# Using the Image

Create a new toolbox using his image with

```
toolbox create -i mbsystem-toolbox $NAME
```

where `$NAME` is what you want to call the toolbox.  Then enter the toolbox the
usual way with

```
toolbox enter $NAME 
```

To install the desktop entry, use:

```
destkop-file-install mbsystem.desktop --dir $HOME/.local/share/applications
mkdir $HOME/.icons
cp MBARI-log.png $HOME/.icons
update-desktop-database $HOME/.local/share/applications
```

However, the toolbox container MUST still be created first using the `toolbox create -i mbsystem-toolbox mbsystem` command.

This entry can be added to the application favorites in gnome with a little work by then running

```
gsettings set org.gnome.shell favorite-apps "$(gsettings get org.gnome.shell favorite-apps | sed s/.$//), 'mbsystem.desktop']"
```
