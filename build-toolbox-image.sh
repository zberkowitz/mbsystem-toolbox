#!/usr/bin/env sh
set -ex
MBSYSTEM_RELEASE=5.8.2beta17

container=$(buildah from fedora-toolbox:41)
# container=fedora-toolbox-working-container

# Install build tools
buildah run $container dnf group install -y development-tools development-libs

# Install MBSystem dependencies
buildah run $container dnf install -y \
    xorg-x11-fonts-misc \
    libtirpc-devel \
    fftw \
    fftw-devel \
    netcdf \
    netcdf-devel \
    nco \
    openmotif \
    openmotif-devel \
    perl \
    python3 \
    cmake3 \
    clang \
    freeglut \
    freeglut-devel \
    mesa-dri-drivers \
    proj \
    proj-devel \
    gdal \
    gdal-devel \
    gdal-python-tools \
    gmt \
    gmt-devel \
    ghostscript \
    opencv-devel

# Other misc nice-to-haves
buildah run $container dnf install -y \
    tmux \
    helix

# Install OTPSnc dependencies
buildah run $container dnf install -y \
    netcdf-fortran-devel

# Install OTPSnc archive and data
buildah add $container OTPSnc-with-tpxo9.tar.bz /opt
# buildah run $container -- sh -c 'rm -rf /opt/OTPSnc/DATA && mv /tmp/DATA /opt/OTPSnc'

## Set up the Model file 
buildah run $container -- sh -c 'cat > /opt/OTPSnc/DATA/Model_tpxo9 <<EOF
/opt/OTPSnc/DATA/h_tpxo9.nc
/opt/OTPSnc/DATA/u_tpxo9.nc
/opt/OTPSnc/DATA/grid_tpxo9.nc
EOF'
buildah run $container -- sed -i 1c\/opt/OTPSnc/DATA/Model_tpxo9 /opt/OTPSnc/setup.inp

## Need to set the correct inclue path for fortran
buildah run $container -- sed -i s@/usr/local/include@/usr/include@ /opt/OTPSnc/makefile

## Make executables and run setup programs
buildah run $container -- sh -c "cd /opt/OTPSnc && make extract_HC predict_tide && ./extract_HC<setup.inp && ./predict_tide<setup.inp"

# MBSystem
## Clone release
buildah run $container -- git clone --branch ${MBSYSTEM_RELEASE} --depth 1 https://github.com/dwcaress/MB-System /tmp/MB-System

## Patch sources to compile
buildah add $container 0001-Change-fonts-for-use-with-Fedora.patch /tmp/MB-System
buildah run $container -- sh -c "cd /tmp/MB-System && patch -p1 < 0001-Change-fonts-for-use-with-Fedora.patch"

## Configure
#buildah run $container -- sh -c "mkdir /tmp/MB-System/build && cd /tmp/MB-System/build && cmake -DotpsDir=/opt/OTPSnc -DbuildTRN=OFF -DbuildOpenCV=ON .. && cmake --build ."
#buildah run $container -- sh -c "cd /tmp/MB-System/build && cmake --install ."

buildah run $container -- sh -c "cmake -DotpsDir=/opt/OTPSnc -DbuildTRN=OFF -DbuildOpenCV=ON -DCMAKE_INSTALL_PREFIX=/usr -S /tmp/MB-System -B /tmp/MB-System/build"
buildah run $container -- sh -c "cmake --build /tmp/MB-System/build"
buildah run $container -- sh -c "cmake --install /tmp/MB-System/build"

# Cleanup
buildah run $container -- sh -c "rm -rf /tmp/MB-System"
buildah run $container -- dnf group remove -y development-tools development-libs

# Commit
buildah commit --rm $container mbsystem-toolbox:${MBSYSTEM_RELEASE}

